var items = [
  {
    type: "text",
    title: "Gold & Blue color scheme",
    description: "We can see this combination of colors on a church dome and right under the icon. Compositionally this woman mirrors The Mother of Jesus with her gold-plated scarf serving as a halo.",
    position: {
      left: 1170,
      top: 200
    }
  },

  {
    type: "text",
    title: "Red color scheme",
    description: "Surikov enframes the sleigh with red from the right on the foreground and from the left on the background. This color, covering Streltsy uniform, charges the composition with additional anxiety.",
    position: {
      left: 875,
      top: 210
    }
  },

    {
    type: "text",
    title: "Gold & Black color scheme",
    description: "Morozova's color scheme here mirrors the icon. Only her heavenly gold background is a hay on the sleigh.",
    position: {
      left: 500,
      top: 460
    }
  },
  
];

var options = {
  allowHtml: true
};

$(document).ready(function() {
  $("#my-interactive-image").interactiveImage(items, options);
});

/* Set the width of the sidebar to 250px (show it) */
function openNav() {
  document.getElementById("mySidepanel").style.width = "400px";
}

/* Set the width of the sidebar to 0 (hide it) */
function closeNav() {
  document.getElementById("mySidepanel").style.width = "0";
} 